# React + Django + PostgreSQL + Django Rest Framework + TypeScript + Next.js + Material ui + Docker Starter Kit

### Requirements

- Docker
- Docker Compose
- NodeJS and NPM

### Execute

Update values as needed

Inside /ui

```npm install```


In root folder:


```docker-compose build```

```docker-compose up```

```docker exec -it backend python ./src/manage.py migrate```

```docker exec -it backend python ./src/manage.py createsuperuser```

```docker-compose exec ui npm install```

### Comands

```docker-compose exec ui npm run dev```

```docker-compose up```

### Open vscode and reopen in container

```

> ui@0.1.0 dev /app
> next dev

ready - started server on http://localhost:3000
```
