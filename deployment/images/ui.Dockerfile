FROM node:13.13.0-alpine3.11

WORKDIR /app

# Install dependecies
COPY ./ui/package.json package.json
COPY ./ui/package-lock.json package-lock.json

RUN apk --no-cache add libpng-dev autoconf automake make g++ libtool nasm \
    && npm ci

COPY ./ui .

RUN npm run build

