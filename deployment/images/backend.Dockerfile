FROM python:3.8.2-alpine3.11

RUN apk add --no-cache musl-dev linux-headers gcc g++ git gettext postgresql-dev jpeg-dev zlib-dev libffi-dev gdal-dev geos-dev proj-dev

ENV PYTHONUNBUFFERED 1
ENV PYTHONFAULTHANDLER 1

WORKDIR /app

COPY backend/requirements.txt .

RUN pip install -r requirements.txt

COPY backend/src ./src

ARG VERSION
ENV VERSION $VERSION


