import {ServerStyleSheets} from '@material-ui/core/styles';
import Document, {Head, Html, Main, NextScript} from 'next/document';
import { theme } from '../theme';

export default class CustomDocument extends Document {
    static async getInitialProps(ctx: any) {
        const sheets = new ServerStyleSheets();
        const originalRenderPage = ctx.renderPage;

        ctx.renderPage = () => originalRenderPage({
            enhanceApp: (App: any) => (props: {}) => sheets.collect(<App {...props} />),
        });

        const initialProps = await Document.getInitialProps(ctx);

        return {
            ...initialProps,
            styles: (
                <>
                    {initialProps.styles}
                    {sheets.getStyleElement()}
                </>
            ),
        };
    }

    render() {
        return (
            <Html lang='es'>
                <Head>
                    <link rel='icon' href='/favicon.ico' />
                    <link rel='manifest' href='/manifest.json' />
                    <link rel='stylesheet' href='https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap' />
                    <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons' />
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <meta name='theme-color' content={theme.palette.primary.main} />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
