import Head from 'next/head'
import {makeStyles} from '@material-ui/core/styles';
import {Typography} from '@material-ui/core';
import { useRouter } from 'next/router';

const useStyles = makeStyles( theme => ({
  klein: {
    color: theme.palette.primary.main,
  },
  container: {
    minHeight: '100vh',
    padding: ' 0 0.5rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#fff',
  },
  main: {
    padding: '5rem 0',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    width: '100 %',
    height: '100px',
    borderTop: '1px solid #eaeaea',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: theme.palette.primary.main,
    textDecoration: 'none',
  },
  title: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    margin: '0',
    lineHeight: '1.15',
    fontSize: '4rem',
    textAlign: 'center',
  },
}));

export default function Home() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={classes.main}>
        <Typography variant='h3' className={classes.klein}>
         I'm happy to see you here ʕ •ᴥ•ʔ
        </Typography>
        <Typography variant='h5'>
         As with all unknown things, <strong>this is going to be an adventure</strong>
        </Typography>
        <Typography variant='h5'>
         - but no worries, since you already worked up the courage to be here, you'll be just fine. 
        </Typography>
        <Typography variant='h5'>
         So, want to learn code but unsure how/where to start? From here you can go quickly and full stack.
        </Typography>
        <Typography variant='h5'>
         <strong>What to do now? Take a break and relax!</strong> You have just done something really huge.
        </Typography>
        
      </main>

      <footer className={classes.footer}>
        <a
          href="https://procnedc.github.io/resume/"
          target="_blank"
          rel="Daniela Colombo"
          className={classes.klein}
        >
          Made with ♥ by DC
        </a>
      </footer>
    </div>
  )
}
