import { api } from 'api';
import { useAuth } from 'contexts/auth';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import * as settings from 'settings';

const Logout: NextPage = ({ }) => {
    const router = useRouter();
    const { setAuthUser } = useAuth();

    const logout = async () => {
        try {
            await api.logout();
        } catch {
            // noop
        }
        setAuthUser(undefined);
        router.push(settings.LOGIN_URL);
    };

    useEffect(() => { logout() }, []);
    return null;
};

export default Logout;
