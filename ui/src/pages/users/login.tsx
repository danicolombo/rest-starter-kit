import { CardContent, CardHeader, Card, Typography } from '@material-ui/core';
import { api } from 'api';
import { useAuth } from 'contexts/auth';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { LoginRequest } from 'types/api';

const Login: NextPage = () => {
    const router = useRouter();
    const { setAuthUser } = useAuth();

    const handleSubmit = async (values: LoginRequest) => {
        // TO-DO: Handle network errors
        const user = await api.login(values);
        setAuthUser(user);
        const nextUrl = (router.query.next as string) || '/';
        router.push(nextUrl);
    };

    // TODO: Improve layout since login is rendered without app menu. For example show the logo here.

    return (
        <Card variant='outlined'>
            <CardHeader
                title='Login'
                subheader='Ingresá tu email y contraseña para acceder a la app.'
            />
            <CardContent>
                <Typography>Hola</Typography>
            </CardContent>
        </Card>
    );
};

export default Login;
