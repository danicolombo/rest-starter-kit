import CssBaseline from '@material-ui/core/CssBaseline';
import {ThemeProvider} from '@material-ui/core/styles';
import Layout from '../components/Layout';
import {AppProps} from 'next/app';
import getConfig from 'next/config';
import Head from 'next/head';
import {useEffect} from 'react';
import * as settings from 'settings';
import {theme} from 'theme';


type CustomAppProps = AppProps & {
    err?: Error;
};

const App = ({Component, pageProps, err}: CustomAppProps) => {
    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles && jssStyles.parentNode) jssStyles.parentNode.removeChild(jssStyles);
    });

    // Workaround for https://github.com/zeit/next.js/issues/8592
    const modifiedPageProps = {...pageProps, err};

    return (
        <>
            <Head>
                <title>{settings.SITE_NAME}</title>
                <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />
            </Head>
            <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <Layout>
                        <Component {...modifiedPageProps} />
                    </Layout>
            </ThemeProvider>
        </>
    );
};

export default App;
