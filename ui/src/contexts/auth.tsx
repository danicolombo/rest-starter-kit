import { CircularProgress } from '@material-ui/core';
import { api } from 'api';
import { useRouter } from 'next/router';
import { ComponentType, createContext, FC, useContext, useEffect, useState } from 'react';
import * as settings from 'settings';
import { User } from '../types/users';

type AuthContextType = {
    authUser: User | undefined;
    setAuthUser(user: User | undefined): void;
};

// context = store
const AuthContext = createContext<AuthContextType>({
    authUser: undefined,
    setAuthUser: () => { },
});

export const AuthProvider: FC = ({ children }) => {
    const [user, setUser] = useState<User | undefined>(undefined);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getActiveUser = async () => {
            try {
                const user = await api.activeUser();
                setUser(user);
            } catch {
                // noop
            }
            setLoading(false);
        };
        getActiveUser();
    }, []);


    return (
        <AuthContext.Provider value={{ authUser: user, setAuthUser: setUser }}>
            {loading ? <CircularProgress color='primary' /> : children}
        </AuthContext.Provider>
    );
};

export const useAuth = () => useContext(AuthContext);

type UserCheck = (user: User) => boolean;

export const userCheckRequired = (check: UserCheck) => (Component: ComponentType) => () => {
    const router = useRouter();
    const { authUser } = useAuth();

    useEffect(() => {
        if (!authUser) router.push(`${settings.LOGIN_URL}?next=${router.pathname}`);
        else if (!check(authUser)) router.push(`${settings.URL}`);
    }, [authUser]);

    return authUser ? <Component /> : null;
};


export const loginRequired = userCheckRequired(user => Boolean(user));

