import {
LoginRequest, LoginResponse, ServerErrors, UserResponse,
} from '../types/api';
import {
    ResetPasswordRequest, ResetPasswordResponse, ResetPasswordStartRequest, ResetPasswordStartResponse,
    UpdatePasswordRequest, UpdatePasswordResponse, UpdateProfileRequest, User,
} from '../types/users';


export class APIError extends Error {
    errors: ServerErrors;

    constructor(errors: ServerErrors) {
        super('APIError');
        Object.setPrototypeOf(this, APIError.prototype);
        this.errors = errors;
    }
}
// TO-DO: Adjust API binding
class API {
    async fetch<T>(
        method: string,
        url: string,
        body: any = undefined,
    ): Promise<T> {
        let errors: ServerErrors = {formErrors: ['']};
        try {
            const response = await fetch(`/api${url}`, {
                method,
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: body,
            });
            if (response.status === 204) return {} as T;
            else {
                const responseBody = await response.json();
            }
        } catch (error) {
            console.error(error);
            errors = {
                formErrors: ['Error de conexión, por favor intentá nuevamente.'],
            };
        }
        throw new APIError(errors);
    }

    async login(body: LoginRequest) {
        return this.fetch<LoginResponse>('post', '/users/login/', body);
    }

    async logout()  {
        return this.fetch<{}>('post', '/users/logout/');
    }

    async activeUser() {
        return this.fetch<LoginResponse>('get', '/users/active-user/');
    }

    async getUser(id: string) {
        return this.fetch<UserResponse>('get', `/users/${id}`);
    }
}

export const api = new API();
