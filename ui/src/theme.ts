import {createMuiTheme} from '@material-ui/core/styles';

const defaultTheme = createMuiTheme({});

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#0008f4',
            light: '#0045f4',
        },
        secondary: {
            main: '#EE7D11',
            light: '#FEF2E0',
        },
        error: {
            main: '#B0001F',
            light: '#FDECEA',
        },
        background: {
            default: '#fff',
        },
    },
});
