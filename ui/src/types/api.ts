import {User} from './users';

// Commons

export type ServerErrors = {
    [field: string]: string[];
};

// Auth

export type LoginRequest = {
    email: string;
    password: string;
};

export type LoginResponse = User;

// Users

export type UserResponse = User;
export type UserRequest = {
    email: string;
    firstName: string;
    lastName: string;
};


