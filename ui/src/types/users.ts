export type User = {
    id: string;
    email: string;
    displayName: string;
    firstName: string;
    lastName: string;
    isActive: boolean;
};

export type ResetPasswordStartRequest = {
    email: string;
};

export type ResetPasswordStartResponse = {};

export type UpdatePasswordRequest = {
    newPassword: string;
    currentPassword: string;
};

export type UpdateProfileRequest = {
    firstName: string;
    lastName: string;
    email: string;
};

export type UpdatePasswordResponse = {};

export type ResetPasswordRequest = {
    password: string;
    user: string;
    token: string;
};

export type ResetPasswordResponse = {};
